<?php
//indicating this person will
//pay the additional share in the bill.
$lines = [
	'40.00 Thijs Danny,Danny,Thijs,Stefan,Den',
	'45.00 Danny Danny,Thijs,Stefan,Den',
	'36.00 Stefan Danny,Thijs,Stefan',
	'40.00 Stefan Danny,Thijs,stefan,Den',
	'40.00 Danny Danny,Thijs,Stefan,Den',
	'12.00 Stefan Thijs,Stefan,Den',
	'44.00 Danny Danny,Thijs,Stefan,Den',
	'42.40 Den Danny,Stefan,Den,Den',
	'40.00 danny Danny,Thijs,Stefan,Den',
	'50.40 Thijs Danny,Thijs,Den',
	'48.00 Den Danny,thijs,Stefan,Den',
	'84.00 Thijs Thijs,Stefan,den'
];

$Calculator = new Calculator($lines);
$Calculator->printBill($lines);

class Calculator{
	//private so people can't mess with bill data calling a property directly on the Calculator class. 
	private $bills = [];
	//constructor gets fed the lines array on creation of the Calculator class
	public function __construct($bills){
		//each line in the lines array gets injected into the BillItem class
		foreach($bills as $bill_item){
			//adds object to end of array
			$this->bills[] = new BillItem($bill_item);
		}
	}

	public function printBill($lines){
		//must be a nested array
		$payout = $this->calculate($lines);

		foreach($payout as $debtor => $lines){
			$debtor = ucfirst($debtor);

			foreach($lines as $creditor => $amount){

				$amount = number_format($amount, 2);
				$creditor = ucfirst($creditor);
				echo "$debtor pays $creditor $amount" . PHP_EOL;
			}
		}
	}

	private function calculate($lines){

		$topay = [];
		$paidTotal = [];
		
		foreach($lines as $line){
			$data = explode(" ",$line);
			$amount = $data[0];
			$payer = ucfirst($data[1]);
			$participants = $data[2];
			$participants = explode(",",$participants);
			$nrOfParticipants = count($participants);
			
			foreach($participants as $participant){
				$participant = ucfirst($participant);
				$share = $amount/$nrOfParticipants;

				//if participant is not the one that paid, add the share to topay array
				if($participant !== $payer){
					$total[$participant][] = $share;
					$topay[$participant][] = array_sum($total[$participant]);
				} else {
					$own[$payer] = $share;

				}
				
			}

			
			$paid[$payer][] = $amount;
			//sum up the amounts paid for lunch per luncher 
			$paidTotal[$payer][] = array_sum($paid[$payer]);
			$ownShare[$payer] = array_sum($own);
			//echo "<pre>".var_dump($paidTotal[$payer])."</pre>";
										
		}

		//sort arrays by key (luncher names)
		ksort($topay);
		ksort($paidTotal);
		ksort($ownShare);
		echo "<h2>Paid in total:</h2>";
		foreach($paidTotal as $key => $value){
			echo "<pre>".$key.": ".end($paidTotal[$key])."</pre>";
		}
		echo "<h2>owes in total:</h2>";
		foreach($topay as $key => $value){
			echo "<pre>".$key.": ".end($topay[$key])."</pre>";
		}
		echo "<h2>own shares:</h2>";
		var_dump($ownShare);
		//echo "<pre>".print_r($topay)."</pre>";
		//echo "<pre>".print_r($paidTotal)."</pre>";
		//return $payout;
	}
}

class BillItem{

	private $price;
	private $paid_by;
	private $attendees = [];

	public function __construct($row){

		$data = explode(' ', $row);
		$this->price = (float) $data[0];
		$this->paid_by = strtolower($data[1]);
		foreach(explode(',', $data[2]) as $debtor){

			$this->attendees[] = strtolower($debtor);
		}
	}
}


?>