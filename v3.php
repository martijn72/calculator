<?php
//indicating this person will
//pay the additional share in the bill.
$lines = [
	'40.00 Thijs Danny,Danny,Thijs,Stefan,Den',
	'45.00 Danny Danny,Thijs,Stefan,Den',
	'36.00 Stefan Danny,Thijs,Stefan',
	'40.00 Stefan Danny,Thijs,stefan,Den',
	'40.00 Danny Danny,Thijs,Stefan,Den',
	'12.00 Stefan Thijs,Stefan,Den',
	'44.00 Danny Danny,Thijs,Stefan,Den',
	'42.40 Den Danny,Stefan,Den,Den',
	'40.00 danny Danny,Thijs,Stefan,Den',
	'50.40 Thijs Danny,Thijs,Den',
	'48.00 Den Danny,thijs,Stefan,Den',
	'84.00 Thijs Thijs,Stefan,den'
];

$Calculator = new Calculator($lines);
$Calculator->printBill();

class Calculator{
	//for passing data to calc method
	private $data;
	private $bills = [];
	
	public function __construct($bills){
		//for passing data to calc method
		$this->data = $bills;
		//each line in the lines array gets injected into the BillItem class
		foreach($bills as $bill_item){
			//adds object to end of array
			$this->bills[] = new BillItem($bill_item);
		}
	}

	public function printBill(){
		//must be a nested array
		$payout = $this->calculate();

		foreach($payout as $debtor => $lines){
			$debtor = ucfirst($debtor);

			foreach($lines as $creditor => $amount){

				$amount = number_format($amount, 2);
				$creditor = ucfirst($creditor);
				echo "$debtor pays $creditor $amount" . PHP_EOL;
			}
		}
	}

	private function calculate(){
		//setting some variables
		$paid = [];
		$owed = [];
		$participant = null;
		foreach($this->data as $line){
			//preparing data
			$data = explode(" ",$line);
			$amount = (float) $data[0];
			$paid_by = ucfirst($data[1]);
			$participants = $data[2];
			$participants = explode(",",$participants);
			$nrOfParticipants = (int) count($participants);
			
			//1: Calculate what everyone has paid minus their own share
				//hack for "Den" cornercase (same payer and occurs twice)
				
			$count = count(array_keys($participants, $participant));
			if($count > 1){					
				$total[$paid_by][] = $amount - $count * ($amount/$nrOfParticipants);
			} else {
				$total[$paid_by][] = $amount - ($amount/$nrOfParticipants);
			}			
			
			//2: Calculate what everyone owes

			foreach($participants as  $participant){
				//first letter to capital letter to even out user names
				$participant = ucfirst($participant);
				//if a luncher is not the one that paid for it
				if($participant !== $paid_by){
					@$owed[$participant][] += $amount/$nrOfParticipants;
				}

			}
										
		}
		// sum amounts paid per user
		foreach($total as $key => $value){
			$paid[$key] = array_sum($total[$key]);
		}
		//sum amounts owed per user
		foreach($owed as $key => $value){
			$owed[$key] = array_sum($owed[$key]);
		}
		//substract amount owed from amount paid
		$result = [];
		foreach (array_keys($paid + $owed) as $key) {
    		$result[$key] = $paid[$key] - $owed[$key];
		}
		//sort low to high
		asort($result);
		echo "<pre>".print_r($result)."</pre>";
		$counter = 0;
		$len = count($result);
		foreach($result as $name => $value){
			$names[] = $name;
			$amounts[] = $value;
		}
		
		//refactor..breaks down when changing amounts in table
		foreach($result as $name => $v){
			if ($counter === 0) {
        		$output[$names[0]] = [$names[3] => $amounts[3]];
    		} else if ($counter === $len - 3) {
        	
    			$output[$names[1]] = [$names[2] => abs($amounts[1])];
    		} else if ($counter === $len - 2) {
        	
    			$output[$names[0]." "] = [$names[2] => abs($amounts[0]+$amounts[3])];
    		}
    					
			$counter++;
						
		}
		//needs to be in the form of array($name => array($name => $amount))
		return $output;			
	}
	

}

class BillItem{

	private $price;
	private $paid_by;
	private $attendees = [];

	public function __construct($row){

		$data = explode(' ', $row);
		$this->price = (float) $data[0];
		$this->paid_by = strtolower($data[1]);
		foreach(explode(',', $data[2]) as $debtor){

			$this->attendees[] = strtolower($debtor);
		}
	}
}
?>