<?php
//indicating this person will
//pay the additional share in the bill.
$lines = [
	'40.00 Thijs Danny,Danny,Thijs,Stefan,Den',
	'45.00 Danny Danny,Thijs,Stefan,Den',
	'36.00 Stefan Danny,Thijs,Stefan',
	'40.00 Stefan Danny,Thijs,stefan,Den',
	'40.00 Danny Danny,Thijs,Stefan,Den',
	'12.00 Stefan Thijs,Stefan,Den',
	'44.00 Danny Danny,Thijs,Stefan,Den',
	'42.40 Den Danny,Stefan,Den,Den',
	'40.00 danny Danny,Thijs,Stefan,Den',
	'50.40 Thijs Danny,Thijs,Den',
	'48.00 Den Danny,thijs,Stefan,Den',
	'84.00 Thijs Thijs,Stefan,den'
];

$Calculator = new Calculator($lines);
$Calculator->printBill();

class Calculator{
	//for passing data to calc method
	private $data;
	private $bills = [];
	
	public function __construct($bills){
		//for passing data to calc method
		$this->data = $bills;
		//each line in the lines array gets injected into the BillItem class
		foreach($bills as $bill_item){
			//adds object to end of array
			$this->bills[] = new BillItem($bill_item);
		}
	}

	public function printBill(){
		//must be a nested array
		$payout = $this->calculate();

		foreach($payout as $debtor => $lines){
			$debtor = ucfirst($debtor);

			foreach($lines as $creditor => $amount){

				$amount = number_format($amount, 2);
				$creditor = ucfirst($creditor);
				echo "$debtor pays $creditor $amount" . PHP_EOL;
			}
		}
	}

	private function calculate(){
		//setting some variables
		$paid = [];
		$owed = [];
		$participant = null;
		foreach($this->data as $line){
			//preparing data
			$data = explode(" ",$line);
			$amount = (float) $data[0];
			$paid_by = ucfirst($data[1]);
			$participants = $data[2];
			$participants = explode(",",$participants);
			$nrOfParticipants = (int) count($participants);
			
			//1: Calculate what everyone has paid minus their own share
				//hack for "Den" cornercase (same payer and occurs twice)
				
			$count = count(array_keys($participants, $participant));
			if($count > 1){					
				$total[$paid_by][] = $amount - $count * ($amount/$nrOfParticipants);
			} else {
				$total[$paid_by][] = $amount - ($amount/$nrOfParticipants);
			}			
			
			//2: Calculate what everyone owes

			foreach($participants as  $participant){
				//first letter to capital letter to even out user names
				$participant = ucfirst($participant);
				//if a luncher is not the one that paid for it
				if($participant !== $paid_by){
					@$owed[$participant][] += $amount/$nrOfParticipants;
				}

			}
										
		}
		// sum amounts paid per user
		foreach($total as $key => $value){
			$paid[$key] = array_sum($total[$key]);
		}
		//sum amounts owed per user
		foreach($owed as $key =>$value){
			$owed[$key] = array_sum($owed[$key]);
		}
		//substract amount owed from amount paid
		$result = [];
		foreach (array_keys($paid + $owed) as $key) {
    		$result[$key] = $paid[$key] - $owed[$key];
		}
		asort($result);
		$original = $result;
		foreach($result as $name => $v){
			//foreach element in the array determine the key of the highest value
			$highest = array_keys($result, max($result));
			//and the key of the lowest value
			$lowest = array_keys($result, min($result));
			//determine how much is left for current debtor to pay to the next creditor
			$remaining = abs(min($result) + (max($result)));

			//get the highest amount since this is the amount that needs to be paid by current debtor to current creditor ie 49.35
			$amount = (max($result));
			//add the name of the creditor and the amount to the current debtor
			$output[$lowest[0]] = [$highest[0] => $amount];
			//$output[$lowest[0]] = [$highest[0] => $remaining];
			//remove the highest and lowest from the array if theres more then two remaining
			if(count($result) > 2){
				array_pop($result);
				array_shift($result);
			}
			$remaining = 0;
			
		}
		

		
		echo "<pre>".print_r($original)."</pre>";
		echo "<pre>".print_r($amount)."</pre>";
		echo "<pre>".print_r($output)."</pre>";
		echo "<pre>".print_r($remaining)."</pre>";
		
		return $output;

		
	}
}

class BillItem{

	private $price;
	private $paid_by;
	private $attendees = [];

	public function __construct($row){

		$data = explode(' ', $row);
		$this->price = (float) $data[0];
		$this->paid_by = strtolower($data[1]);
		foreach(explode(',', $data[2]) as $debtor){

			$this->attendees[] = strtolower($debtor);
		}
	}
}

?>